SHELL=/bin/bash
EXECUTE=poetry run
VERSION=$$(poetry version -s)

PACKAGE=hubchk

TEST_GROUP=tests/
LINT_GROUP=$(PACKAGE)
CLEAN_GROUP=$(PACKAGE) tests

# Standard targets
all: build

install:
	python3.11 -m pip install ./dist/$(PACKAGE)-$(VERSION)-py3-none-any.whl

uninstall:
	python3.11 -m pip uninstall $(PACKAGE)

clean: dev
	rm -rf dist .pytest_cache poetry.lock
	$(EXECUTE) pyclean $(CLEAN_GROUP)

info:

check: lint test

# less standard
dev:
	source .env
	poetry env use python3.11
	poetry install

format:

lint: dev
	$(EXECUTE) ruff $(LINT_GROUP) $(TEST_GROUP)
	$(EXECUTE) mypy $(LINT_GROUP)

test: dev
	$(EXECUTE) pytest $(TEST_GROUP) -vv

coverage: dev
	$(EXECUTE) pytest --cov=$(PACKAGE) $(TEST_GROUP) --cov-report term-missing

build:
	poetry env use python3.11
	poetry build

fresh: clean dev
	poetry update
