# IP Lookup

Design considerations.

## Saas - ipify

**Pros**

- free

**Cons**

- Slowest
- Unknown infrastructure / tracking


```
     time_namelookup:  0.008493s
        time_connect:  0.131465s
     time_appconnect:  0.294404s
    time_pretransfer:  0.294531s
       time_redirect:  0.000000s
  time_starttransfer:  0.417878s
                     ----------
          time_total:  0.418063s
```

## Cloudflare hosted worker

Rate limiting not a blocker
but if the route is exposed could pose a threat vector.

**Pros**

- 5x Quicker
- Simple
- Own source code

**Cons**

- Unknown infrastructure
- Rate limited on free version

```
     time_namelookup:  0.016402s
        time_connect:  0.024831s
     time_appconnect:  0.078765s
    time_pretransfer:  0.079208s
       time_redirect:  0.000000s
  time_starttransfer:  0.095159s
                     ----------
          time_total:  0.095333s
```

## Router

**Pros**

- Available on the same network.
- Quick - 50x quicker than ipify.com

**Cons**

- hardware specific
- upgrade mechanism out of user control

```
     time_namelookup:  0.000073s
        time_connect:  0.001185s
     time_appconnect:  0.000000s
    time_pretransfer:  0.001283s
       time_redirect:  0.000000s
  time_starttransfer:  0.009454s
                     ----------
          time_total:  0.009634s
```
