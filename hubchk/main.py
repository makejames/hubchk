"""Main instance of HubChk."""

import sys

from loguru import logger

from hubchk.cloudflare import Cloudflare
from hubchk.file_handler import ReadFile, WriteFile
from hubchk.hub import PublicIPAddress


def no_known_ip(current_ip):
    """Exit gracefully."""
    logger.info("No recorded IP, exiting script.")
    _new_ip = ReadFile().content
    if _new_ip == current_ip:
        logger.info("New record matches, next run will validate changes.")
        sys.exit(0)
    logger.warning("New IP does not match, write failed.")
    sys.exit(1)

logger.info("Starting script.")
previous_ip = ReadFile().content
current_ip = f"{PublicIPAddress()}"

WriteFile(current_ip).write()

if previous_ip is None:
    no_known_ip(current_ip)

if previous_ip == current_ip:
    logger.info("Success, no change.")
    ReadFile().content
    sys.exit(0)

cf = Cloudflare(previous_ip, current_ip)
cf.update_hosts()
