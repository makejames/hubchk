"""Create package models."""

from dataclasses import dataclass
from ipaddress import IPv4Address
from typing import List

from pydantic import BaseModel, ConfigDict


class IpAddress(BaseModel):
    """IP Address validation."""

    ip: IPv4Address


@dataclass
class DNSRecord:
    """Create an instance of a DNS record."""

    id: str
    name: str
    type: str
    content: str
    comment: str | None


class ResponseError(BaseModel):
    """Response Error structure."""

    code: int
    message: str


class BaseResponse(BaseModel):
    """Base Response."""

    success: bool
    errors: List[ResponseError | None]

    model_config = ConfigDict(extra='ignore')


class ResponseInfo(BaseModel):
    """Stats about the response."""

    page: int
    per_page: int
    count: int
    total_count: int
    total_pages: int


class Record(BaseModel):
    """Individual host record."""

    id: str
    zone_id: str
    zone_name: str
    name: str
    type: str
    content: str
    proxied: bool
    locked: bool
    comment: str | None
    tags: List[str | None]
    created_on: str
    modified_on: str


class UpdateRecord(BaseModel):
    """Update Record."""

    content: str
    name: str
    proxied: bool
    type: str
    comment: str


class SingleResponse(BaseResponse):
    """Response from a put request."""

    result: Record


class Response(BaseResponse):
    """Complete Response."""

    result: List[Record]
    result_info: ResponseInfo
