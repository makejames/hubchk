"""Manage app state."""

from ipaddress import AddressValueError, IPv4Address
from typing import Any

from loguru import logger


class File:
    """Create an instance of the default file path."""

    _default_path: str = "/tmp/dynamic-ip.txt"

    def __init__(self) -> None:
        """Set class variables."""
        self.path: str = self._path()

    def _path(self) -> str:
        return self._default_path

    def __str__(self) -> str:
        """Return the path string."""
        return self.path


class ReadFile:
    """Read current file contents."""

    def __init__(self) -> None:
        """Set class variables."""
        _content = self._read()
        self.content: str | None = self._validate_content(_content)

    @logger.catch
    def _read(self) -> Any:
        """Read files.
        Current method catches all errors.
        Files with elevated permissions will return None.
        They will also fail to write.
        This will propagate to necessary calls to the DNS provider.
        """
        path = f"{File()}"
        logger.debug(f"File path: {path}")
        with open(path, "r") as file:
            content = file.read()
        return content

    def _validate_content(self, content) -> str | None:
        if isinstance(content, str):
            try:
                IPv4Address(content)
                logger.debug(f"file content: {content}")
                return content
            except AddressValueError:
                logger.debug("Un-addressable file content.")
        return None


class WriteFile:
    """Write to path."""

    def __init__(self, content: str) -> None:
        """Set class variables."""
        self.content = content

    def write(self) -> None:
        """Commit data to file."""
        path = f"{File()}"
        with open(path, "w") as file:
            file.write(self.content)

    def validate(self) -> bool:
        """Validate that the file has been written successfully."""
        if self.content == ReadFile().content:
            return True
        logger.error("Write failed.")
        return False
