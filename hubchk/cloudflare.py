"""Manage calls to cloudflare."""

import os
import sys
from datetime import datetime
from typing import List

import requests
from loguru import logger

from hubchk.models import (BaseResponse, Record, Response, SingleResponse,
                           UpdateRecord)


class Cloudflare:
    """Uility class for calls to cloudflare."""

    api: str = "https://api.cloudflare.com/client/v4/"
    auth: str = os.getenv("AUTH", default="")
    zone: str = os.getenv("ZONE", default="")


    def __init__(self, old_ip: str | None, new_ip: str) -> None:
        """Instantiate the class."""
        self.old_ip = old_ip
        self.new_ip = new_ip
        self.hosts = self.fetch_hosts()

    def fetch_hosts(self) -> List[Record] | None:
        """Retrieve hosts."""
        params = f"?type=A&content={self.old_ip}"
        url = f"{self.api}zones/{self.zone}/dns_records{params}"
        headers = {
            "Authorization": f"Bearer {self.auth}",
            "Content-Type": "application/json",
        }
        _response = requests.get(url=url, headers=headers)
        logger.debug(f"STATUS: {_response.status_code} - {_response.reason}")

        if _response.status_code >= 400:
            response = BaseResponse(**_response.json())
            if response.errors[0]:
                logger.warning(f"Success: {response.success} - "
                               f"Message: {response.errors[0].message}")
            else:
                logger.critical(f"Response code {_response.status_code} "
                                f"Unknown error.")
            return None

        response = Response(**_response.json())
        logger.info(f"Success: {response.success} - "
                    f"returned {response.result_info.total_count} records")

        if len(response.result) == 0:
            return None

        return response.result

    def update_hosts(self) -> None:
        """Update records."""
        base_url = f"{self.api}zones/{self.zone}/dns_records/"
        headers = {
            "Authorization": f"Bearer {self.auth}",
            "Content-Type": "application/json",
        }

        if self.hosts is None:
            logger.warning("Nothing to update.")
            return None

        for record in self.hosts:
            url = f"{base_url}{record.id}"

            payload = UpdateRecord(
                content=self.new_ip,
                name=record.name,
                proxied=record.proxied,
                type=record.type,
                comment=f"Auto Updated. Last updated {datetime.utcnow()}"
            )

            logger.debug(f"Updating {payload.name}: {payload.content}")

            r = requests.put(url, headers=headers, json=payload.model_dump())

            logger.debug(f"STATUS: {r.status_code} - {r.reason}")

            if r.status_code >= 400:
                response = BaseResponse(**r.json())
                if response.errors[0]:
                    logger.critical(f"Success: {response.success} - "
                                    f"Message: {response.errors[0].message} - "
                                    f"Update to {payload.name} failed")
                else:
                    logger.critical("Unknown Error.")
                sys.exit(1)

            response = SingleResponse(**r.json())

            logger.info(f"Success: {response.success} - "
                        f"Updated {response.result.name}, "
                        f"New content {response.result.content}")
