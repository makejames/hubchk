"""Manage Home Hub device info."""

import re
from ipaddress import IPv4Address

import requests
from loguru import logger


class Hub:
    """Declare the location of the Hub."""
    default_ip_address = IPv4Address("192.168.1.254")

    def __init__(self) -> None:
        """Instantiate the class."""
        self.ip_address: IPv4Address = self.default_ip_address
        self.stats: str = f"http://{self.ip_address}/cgi/cgi_basicStatus.js"


class PublicIPAddress:
    """Retrieve Public IP address"""

    def __init__(self) -> None:
        """Instantiate the class."""
        self.public_ip_address: IPv4Address = self._fetch_ip4_info()
        self._check_ip_address_in_range()

    def _fetch_ip4_info(self) -> IPv4Address:
        hub_info = requests.put(Hub().stats)
        ip4_address = re.findall(
            r"\b\d{1,3}\%2E\d{1,3}\%2E\d{1,3}\%2E\d{1,3}\b",
            hub_info.text
        )[0]
        ip4_address = re.sub(
            r"\%2E",
            ".",
            ip4_address,
        )
        logger.debug(f"Fetched IP address: {ip4_address}")
        return IPv4Address(ip4_address)

    def _check_ip_address_in_range(self) -> None:
        if self.public_ip_address == IPv4Address("0.0.0.0"):
            raise ConnectionError("Home Hub is currently disconnected "
                                  "and without public ip.")

    def __str__(self) -> str:
        """Return the IP address as a string."""
        return f"{self.public_ip_address}"
