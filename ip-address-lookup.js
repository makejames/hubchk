/* Simple, deployable javascript to return public ip address of the caller.
*/

addEventListener('fetch', event => {
  event.respondWith(fetchAndRespond(event.request))
})

async function fetchAndRespond(request) {

    let reqUrl = new URL(request.url).pathname

    let ip = request.headers.get('Cf-Connecting-Ip')

    if (reqUrl == '/') {
      return new Response(
        ip, 
        {
          headers: {
            'Content-Type': 'text/plain'
          },
          status: 200,
          statusText: "SUCCESS",
        }
      )
    } else {
      return new Response(
        "404: Not found",
        {
          headers: {
            'Content-Type': 'text/plain'
          },
          status: 404,
          statusText: "NOT FOUND",
        }
      )
    }
}
