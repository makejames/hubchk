# HubChk

HubChk is a dynamic IP address updater for DNS.

Dynamically assigned Public IP addresses can be difficult to manage.
There are several D-DNS services that are available,
but most of these require installation on the router.


## Design

### Pre-requisites

- BT Home Hub
- API access to DNS provider.

### POC

Initial proof of concept (POC) design of this app can be seen here.

![process-daigram](docs/HubChk.drawio.png)
