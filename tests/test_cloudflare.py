"""Test Calls to Cloudflare."""

import json
from unittest.mock import Mock

import pytest
from requests.exceptions import HTTPError

from hubchk.cloudflare import Cloudflare


class TestCloudflare:
    """Test the Utility methods of the Cloudflare class."""

    @pytest.fixture
    def mock_get(self, mocker) -> Mock:
        """Mock requests."""
        mock = Mock()
        mocker.patch("requests.get", return_value=mock)
        return mock

    @pytest.fixture
    def mock_put(self, mocker) -> Mock:
        """Mock requests."""
        mock = Mock()
        mocker.patch("requests.put", return_value=mock)
        return mock

    @pytest.mark.parametrize(
        "status,reason,file,expected",
        [
            [
                200,
                "OK",
                "tests/responses/cloudflare/example-get.json",
                "test.example.com"
            ],
            [
                400,
                "BAD REQUEST",
                "tests/responses/cloudflare/auth-failure.json",
                None
            ],
            [
                200,
                "OK",
                "tests/responses/cloudflare/no-results.json",
                None
            ],
            [
                400,
                "BAD REQUEST",
                "tests/responses/cloudflare/zone-error.json",
                None
            ],
        ],
    )
    def test_when_response_valid_then_pass(self, mock_get, status, reason,
                                           file, expected) -> None:
        """R-BICEP: Right."""
        with open(file, "r") as f:
            _data = json.loads(f.read())

        def _json() -> dict:
            return _data

        mock_get.status_code = status
        mock_get.reason = reason

        mock_get.json = _json
        test = Cloudflare("127.0.0.1", "127.0.0.2")
        if expected:
            assert test.hosts[0].name == expected
        else:
            assert test.hosts is None

    @pytest.mark.parametrize(
        "status,reason,get_response",
        [
            [
                400,
                "BAD REQUEST",
                "tests/responses/cloudflare/auth-failure.json",
            ],
            [
                200,
                "BAD REQUEST",
                "tests/responses/cloudflare/no-results.json",
            ],
        ],
    )
    def test_when_hosts_none_then_put_not_called(self, mock_get, mock_put,
                                                 status, reason,
                                                 get_response) -> None:
        """R-BICEP: Right."""
        with open(get_response, "r") as f:
            _data = json.loads(f.read())

        def _json() -> dict:
            return _data

        mock_get.status_code = status
        mock_get.reason = reason
        mock_get.json = _json

        test = Cloudflare("127.0.0.1", "127.0.0.2")
        test.update_hosts()
        mock_put.assert_not_called()

    @pytest.mark.parametrize(
        "status,reason,response",
        [
            [
                [200, 400],
                ["OK", "BAD REQUEST"],
                [
                    "tests/responses/cloudflare/example-get.json",
                    "tests/responses/cloudflare/auth-failure.json",
                ]
            ],
        ],
    )
    def test_when_auth_wrong_in_put_none_then_sys_exit(
        self,
        mock_get,
        mock_put,
        status,
        reason,
        response
    ) -> None:
        """R-BICEP: Right."""
        with open(response[0], "r") as f:
            get_data = json.loads(f.read())

        with open(response[1], "r") as f:
            put_data = json.loads(f.read())

        def get_json() -> dict:
            return get_data

        def put_json() -> dict:
            return put_data

        mock_get.status_code = status[0]
        mock_get.reason = reason[0]
        mock_get.json = get_json

        mock_put.status_code = status[1]
        mock_put.reason = reason[1]
        mock_put.json = put_json
        mock_put.raise_for_status = HTTPError

        test = Cloudflare("127.0.0.1", "127.0.0.2")
        with pytest.raises(SystemExit):
            test.update_hosts()

    @pytest.mark.parametrize(
        "status,reason,response",
        [
            [
                [200, 200],
                ["OK", "OK"],
                [
                    "tests/responses/cloudflare/example-get.json",
                    "tests/responses/cloudflare/example-put.json",
                ]
            ],
        ],
    )
    def test_when_200_responses_then_pass(
        self,
        mock_get,
        mock_put,
        status,
        reason,
        response,
        capsys,
    ) -> None:
        """R-BICEP: Right."""
        with open(response[0], "r") as f:
            get_data = json.loads(f.read())

        with open(response[1], "r") as f:
            put_data = json.loads(f.read())

        def get_json() -> dict:
            return get_data

        def put_json() -> dict:
            return put_data

        mock_get.status_code = status[0]
        mock_get.reason = reason[0]
        mock_get.json = get_json

        mock_put.status_code = status[1]
        mock_put.reason = reason[1]
        mock_put.json = put_json
        mock_put.raise_for_status = HTTPError

        capsys.readouterr()
        test = Cloudflare("127.0.0.1", "127.0.0.2")
        test.update_hosts()
