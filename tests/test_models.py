"""Test Models."""

from ipaddress import IPv4Address

import pytest
from pydantic import ValidationError

from hubchk import models


class TestIpAddress:
    """Test the validation of the IpAddress class."""

    @pytest.mark.parametrize(
        "value,expected",
        [
            ("127.0.0.1", "127.0.0.1"),
            ("0.0.0.0", "0.0.0.0"),
        ],
    )
    def test_when_valid_pass(self, value, expected):
        """R-BICEP: Right."""
        _ip = models.IpAddress(ip=value)
        assert format(_ip.ip) == expected
        assert isinstance(_ip.ip, IPv4Address)

    @pytest.mark.parametrize(
        "value",
        [
            (None),
            ("example.com"),
            (""),
        ],
    )
    def test_when_invalid_ip_raise_value_error(self, value):
        """R-BICEP: Error."""
        with pytest.raises(ValidationError):
            models.IpAddress(ip=value)
