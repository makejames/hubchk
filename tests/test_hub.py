"""Test Home Hub device Info."""

from unittest.mock import Mock

import pytest

from hubchk.hub import Hub, PublicIPAddress


class TestHub:
    """Test the methods of the Hub class."""

    def test_default_ip_is_mapped(self) -> None:
        """R-BICEP: Right."""
        test = Hub()
        assert test.default_ip_address == test.ip_address

    def test_stats_string_contains_ip_address(self) -> None:
        """R-BICEP: Right."""
        test = Hub()
        assert format(test.ip_address) in test.stats


class TestPublicIPAddress:
    """Test the methods of the PublicIPAddress class."""

    @pytest.fixture
    def mock_response(self, mocker) -> Mock:
        """Mock requests."""
        mock = Mock()
        mocker.patch("requests.put", return_value=mock)
        return mock

    def test_when_connected_return_ip_address(self, mock_response) -> None:
        """R-BICEP: Right."""
        with open("tests/responses/bt/basicStatusConnected.js",
                  mode="r") as file:
            response = file.read()
        mock_response.status_code = 200
        mock_response.text = response
        assert format(PublicIPAddress()) == "203.0.113.210"

    def test_when_disconnected_raise_error(self, mock_response) -> None:
        """R-BICEP: Error."""
        with open("tests/responses/bt/basicStatusDisconnected.js",
                  mode="r") as file:
            response = file.read()
        mock_response.status_code = 200
        mock_response.text = response
        with pytest.raises(ConnectionError):
            PublicIPAddress()

    def test_when_nothing_returned_raise_error(self, mock_response) -> None:
        """R-BICEP: Error."""
        mock_response.status_code = 404
        mock_response.text = ""
        with pytest.raises(IndexError):
            PublicIPAddress()
