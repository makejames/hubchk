# Cloudflare Responses

Example cloudflare responses.
This directory contains several examples of api responses,
including error responses.

## Auth Failure

With a 400 error code,
auth failures are generated when a token in invalid
or a request is improperly formatted

## examples

Example get and put responses are included.


## no-results

If a query parameter returns a no-bananas 200 response,
the results field will be an empty list

## zone-error

Requests, otherwise correctly formatted,
but with an invalid zone, will return a null results field.
