"""Test the methods of the file_handler."""

from typing import Any

import pytest

from hubchk.file_handler import File, ReadFile, WriteFile


class TestFile:
    """Test the methods of the File class."""

    def test_when_called_default_path_returned(self) -> None:
        """R-BICEP: Right."""
        assert f"{File()}" == "/tmp/dynamic-ip.txt"

class TestReadFile:
    """Test the methods of the ReadFile class."""

    @pytest.fixture
    def mock_file(self, mocker, request) -> None:
        """Mock File class."""
        def _path(self):
            return request.param
        mocker.patch("hubchk.file_handler.File._path",
                     new=_path)

    @pytest.fixture
    def expected(self, request) -> Any:
        """Return content text."""
        return request.param

    @pytest.fixture
    def mock_read(self, mocker, request) -> Any:
        """Mock read."""
        def _read(self):
            return request.param
        mocker.patch("hubchk.file_handler.ReadFile._read",
                     new=_read)
        return request.param

    @pytest.mark.parametrize(
        "mock_file, expected",
        [
            ["tests/data/valid.txt", "127.0.0.1"],
            # ["tests/data/permission_error.txt", None],
            ["tests/data/file_not_fount.txt", None],
            ["tests/data/directory_error.txt", None]
        ],
        indirect=True
    )
    def test_file_pass(self, mock_file, expected) -> None:
        """R-BICEP: Right."""

        assert ReadFile().content == expected

    @pytest.mark.parametrize(
        "mock_read, expected",
        [
            ["127.0.0.1", "127.0.0.1"],
            ["test", None],
            [127001, None],
            [True, None]
        ],
        indirect=True
    )
    def test_content_validated(self, mock_read, expected) -> None:
        """R-BICEP: Right."""
        assert ReadFile().content == expected


class TestWriteFile:
    """Test the methods of the WriteFile class."""

    @pytest.fixture(scope="session")
    def mock_file(self, tmp_path_factory) -> Any:
        """Mock File class."""
        return tmp_path_factory.mktemp('data') / 'dynamic_ip.txt'

    @pytest.mark.parametrize(
        "test_data,",
        [
            "127.0.0.1",
            "0.0.0.0"
        ]
    )
    def test_file_write(self, mock_file, test_data, mocker):
        """R-BICEP: Right."""
        def _path(self):
            return f"{mock_file}"
        mocker.patch("hubchk.file_handler.File._path", new=_path)
        test = WriteFile(test_data)
        test.write()
        assert test.validate() is True

    # def test_file_permission_error_false(self, mocker) -> None:
    #     """R-BICEP: Right."""
    #     def _path(self):
    #         return "tests/data/permission_error.txt"
    #     mocker.patch("hubchk.file_handler.File._path", new=_path)
    #     with pytest.raises(PermissionError):
    #         WriteFile("0.0.0.0").write()
